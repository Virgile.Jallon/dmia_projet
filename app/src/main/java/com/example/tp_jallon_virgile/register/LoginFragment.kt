package com.example.tp_jallon_virgile.register

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.tp_jallon_virgile.MainActivity
import com.example.tp_jallon_virgile.R
import com.example.tp_jallon_virgile.databinding.FragmentAuthenticationBinding
import com.example.tp_jallon_virgile.databinding.FragmentLoginBinding
import com.example.tp_jallon_virgile.network.Api
import kotlinx.coroutines.launch

/***
 * Permet à l'utilisateur de se connecter
 */
class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    // True si les champs email et password ne sont pas vides
    fun areFieldsCompleted(): Boolean {
        return binding.email.text.toString() != "" && binding.password.text.toString() != ""
    }

    // Permet à l'utilisateur de se connecter via la complétion d'un formulaire
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnLogin.setOnClickListener {
            if(areFieldsCompleted()){
                // Les champs sont complétés convenablement
                val loginForm = LoginForm(
                    binding.email.text.toString(),
                    binding.password.text.toString()
                )
                // Interroge le serveur via l'API
                lifecycleScope.launch {
                    val response = Api.userWebService.login(loginForm)
                    if(response.isSuccessful && response.body() != null){
                        // Récupère le retour de l'API
                        val SHARED_PREF_TOKEN_KEY = "auth_token_key"
                        val preferences = activity!!.getSharedPreferences("MY_APP", Context.MODE_PRIVATE)
                        preferences.edit().putString(SHARED_PREF_TOKEN_KEY, response.body()!!.token).apply()
                    }else{
                        // Une erreur de connexion est survenue
                        Toast.makeText(context, "Erreur de connexion", Toast.LENGTH_LONG).show()
                    }
                }
            }else{
                // Les champs ne sont pas complétés convenablement
                Toast.makeText(context, "Veuillez remplir les champs", Toast.LENGTH_SHORT).show()
            }
        }
    }

}