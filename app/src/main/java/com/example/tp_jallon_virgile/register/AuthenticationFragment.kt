package com.example.tp_jallon_virgile.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.tp_jallon_virgile.R
import com.example.tp_jallon_virgile.databinding.FragmentAuthenticationBinding

/**
 * Permet à l'utiateur de se connecter ou de créer un compte
 */
class AuthenticationFragment : Fragment() {
    private var _binding: FragmentAuthenticationBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAuthenticationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Définit le comportement à adopter lors d'un clic sur l'un des deux boutons
        binding.btnLogin.setOnClickListener {
            // findNavController().navigate(R.id.action_authenticationFragment_to_loginFragment)
        }
        binding.btnSignup.setOnClickListener {
            // findNavController().navigate(R.id.action_authenticationFragment_to_signupFragment)
        }
    }
}