package com.example.tp_jallon_virgile.register

import kotlinx.serialization.SerialName

data class LoginForm(
    @SerialName("email")
    val email: String,
    @SerialName("password")
    val password: String
)
