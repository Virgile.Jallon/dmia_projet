package com.example.tp_jallon_virgile.register

import kotlinx.serialization.SerialName

data class LoginResponse(
    @SerialName("token")
    val token: String
)
