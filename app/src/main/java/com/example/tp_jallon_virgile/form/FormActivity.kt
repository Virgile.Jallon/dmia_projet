package com.example.tp_jallon_virgile.form
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.example.tp_jallon_virgile.R
import com.example.tp_jallon_virgile.tasklist.Task
import java.util.*

/*{
  "firstname": "didier",
  "lastname": "raoul",
  "email": "didier.raoul@gmail.com",
  "password": "plops$$$",
  "password_confirmation": "plops$$$"

  eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1NDgsImV4cCI6MTY3MDMzMjU5MH0.vrLUmS1HF2YQzVnnVJZG-P5ncnSJz62WSI1jVTPWtDs
}*/

/**
 * Formulaire permettant de modifier une tâche
 * La tâche à modifier est passée en paramètre par un Intent
 */
class FormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        // Récupère la task passée en paramètre par l'intent
        val editTask = intent.getSerializableExtra("task") as Task?
        val saveEditButton = findViewById<Button>(R.id.buttonSaveEdit);

        // Valorise les champs texte selon le texte associé à la task
        val editTextTitle =  findViewById<EditText>(R.id.plainTextTitle)
        editTextTitle.setText(editTask?.title)
        val editTextDesc =  findViewById<EditText>(R.id.plainTextDescription)
        editTextDesc.setText(editTask?.description)

        // Sauvegarde les modifications
        saveEditButton.setOnClickListener {
            // Si la tâche n'a pas d'id, lui en donne un aléatoire
            val id = editTask?.id ?: UUID.randomUUID().toString()
            // Crée la tâche qui représente notre tâche modifiée
            val newTask = Task(id, editTextTitle.text.toString(), editTextDesc.text.toString())
            intent.putExtra("task", newTask)
            setResult(RESULT_OK, intent)
            finish()
        }
    }
}