package com.example.tp_jallon_virgile.user

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import com.example.tp_jallon_virgile.R
import com.example.tp_jallon_virgile.databinding.ActivityUserInfoBinding
import com.example.tp_jallon_virgile.databinding.FragmentTaskListBinding
import com.example.tp_jallon_virgile.network.Api
import com.example.tp_jallon_virgile.network.UserWebService
import com.google.android.material.snackbar.Snackbar
import com.google.modernstorage.mediastore.FileType
import com.google.modernstorage.mediastore.MediaStoreRepository
import com.google.modernstorage.mediastore.SharedPrimary
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

/**
 * Contient des méthodes en lien ave l'utilisateur.
 * Ex : prendre une photo, importer une photo, demander des permissions, ...
 * */
class UserInfoActivity : AppCompatActivity() {

    private val userWebService = Api.userWebService
    private lateinit var binding: ActivityUserInfoBinding

    val mediaStore by lazy { MediaStoreRepository(this) }
    // créer une uri pour sauvegarder l'image, dans onViewCreated
    private lateinit var photoUri: Uri


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_user_info)
        binding = ActivityUserInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Définit le comportement des boutons
        val takePictureButton = binding.takePictureButton
        takePictureButton.setOnClickListener{
            launchCameraWithPermission()
        }
        val uploadImageButton = binding.uploadImageButton
        uploadImageButton.setOnClickListener {
            galleryLauncher.launch("image/*")
        }
        lifecycleScope.launchWhenStarted {
            photoUri = mediaStore.createMediaUri(
                filename = "picture.jpg",
                type = FileType.IMAGE,
                location = SharedPrimary
            ).getOrThrow()
        }
    }

    private val cameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { accepted ->
            if (accepted) launchCamera()
            else showExplanation()
        }

    private fun launchCameraWithPermission() {
        val camPermission = Manifest.permission.CAMERA
        val permissionStatus = checkSelfPermission(camPermission)
        val isAlreadyAccepted = permissionStatus == PackageManager.PERMISSION_GRANTED
        val isExplanationNeeded = shouldShowRequestPermissionRationale(camPermission)
        when {
            isAlreadyAccepted -> launchCamera()
            isExplanationNeeded -> showExplanation()
            else -> cameraPermissionLauncher.launch(camPermission)
        }
    }

    private fun showExplanation() {
        // ici on construit une pop-up système (Dialog) pour expliquer la nécessité de la demande de permission
        AlertDialog.Builder(this)
            .setMessage("🥺 On a besoin de la caméra, vraiment! 👉👈")
            .setPositiveButton("Bon, ok") { _, _ ->  launchAppSettings()}
            .setNegativeButton("Nope") { dialog, _ -> dialog.dismiss()}
            .show()
    }

    private fun launchAppSettings() {
        // Cet intent permet d'ouvrir les paramètres de l'app (pour modifier les permissions déjà refusées par ex)
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", this.packageName, null)
        )
        // ici pas besoin de vérifier avant car on vise un écran système:
        startActivity(intent)
    }

    private fun handleImage(imageUri: Uri) {
        // Dans handleImage, envoyez l'image au serveur avec updateAvatar et convert
        lifecycleScope.launch {
            val updateAvatar = userWebService.updateAvatar(convert(imageUri))
        }

    }



    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { accepted ->
            if (accepted) handleImage(photoUri)
            else Snackbar.make(binding.root, "Échec!", Snackbar.LENGTH_LONG)
        }

    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
        if (uri != null) {
            handleImage(uri)
        } else {
            // Erreur
            Snackbar.make(binding.root, "Échec!", Snackbar.LENGTH_LONG)
        }
    }


    private fun launchCamera() {
        cameraLauncher.launch(photoUri)
    }

    private fun convert(uri: Uri): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            name = "avatar",
            filename = "temp.jpeg",
            body = contentResolver.openInputStream(uri)!!.readBytes().toRequestBody()
        )
    }
}