package com.example.tp_jallon_virgile.network

import com.example.tp_jallon_virgile.register.LoginForm
import com.example.tp_jallon_virgile.register.LoginResponse
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

/**
 * Signature des méthodes permettant de faire appel à l'API
 */
interface UserWebService {
    @GET("users/info")
    suspend fun getInfo(): Response<UserInfo>

    @Multipart
    @PATCH("users/update_avatar")
    suspend fun updateAvatar(@Part avatar: MultipartBody.Part): Response<UserInfo>

    @POST("users/login")
    suspend fun login(@Body user: LoginForm): Response<LoginResponse>
}