package com.example.tp_jallon_virgile.network

import com.example.tp_jallon_virgile.tasklist.Task
import retrofit2.Response
import retrofit2.http.*

/**
 * Signature des méthodes permettant de faire appel à l'API
 */
interface TasksWebService {
    @GET("tasks")
    suspend fun getTasks(): Response<List<Task>>

    @POST("tasks")
    suspend fun create(@Body task: Task): Response<Task>

    @PATCH("tasks/{id}")
    suspend fun update(@Body task: Task, @Path("id") id: String? = task.id): Response<Task>

    @DELETE("tasks/{id}")
    suspend fun delete(@Path("id") id: String): Response<Unit>
}