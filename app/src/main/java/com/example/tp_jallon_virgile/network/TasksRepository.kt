package com.example.tp_jallon_virgile.network

import com.example.tp_jallon_virgile.tasklist.Task
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

/**
 * MANAGER - Permet de faire des requêtes via l'API
 * Permet de faire un CRUD des différentes tâches
 */
class TasksRepository {
    private val tasksWebService = Api.tasksWebService

    // Ces deux variables encapsulent la même donnée:
    // [_taskList] est modifiable mais privée donc inaccessible à l'extérieur de cette classe
    private val _taskList = MutableStateFlow<List<Task>>(value = emptyList())
    // [taskList] est publique mais non-modifiable:
    // On pourra seulement l'observer (s'y abonner) depuis d'autres classes
    public val taskList: StateFlow<List<Task>> = _taskList.asStateFlow()

    /**
     * Récupère la liste des tâches via l'API
     * Retourne la réponse de l'API si la requête est un succès. Null sinon.
     */
    suspend fun loadTasks(): List<Task>? {
        val response = tasksWebService.getTasks()
        return if (response.isSuccessful) response.body() else null
    }

    /**
     * Met à jour une tâche via l'API
     * Retourne la réponse de l'API si la requête est un succès. Null sinon.
     */
    suspend fun updateTask(task: Task): Task? {
        val response = tasksWebService.update(task, task.id)
        return if (response.isSuccessful) response.body() else null
    }

    /**
     * Supprime une tâche via l'API
     * Retourne la réponse de l'API si la requête est un succès. Null sinon.
     */
    suspend fun deleteTask(task: Task): Boolean {
        val response = tasksWebService.delete(task.id)
        return response.isSuccessful
    }

    /**
     * Crée une tâche via l'API
     * Retourne la réponse de l'API si la requête est un succès. Null sinon.
     */
    suspend fun createTask(task: Task): Task? {
        val response = tasksWebService.create(task)
        return if (response.isSuccessful) response.body() else null
    }
}