package com.example.tp_jallon_virgile.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Classe représentant un utilisateur
 */
@Serializable
data class UserInfo(
    @SerialName("email")
    val email: String,
    @SerialName("firstname")
    val firstName: String,
    @SerialName("lastname")
    val lastName: String,
    @SerialName("avatar")
    val avatar: String?
)
