package com.example.tp_jallon_virgile.tasklist
import TaskListAdapter
import TaskListListener
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_jallon_virgile.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.util.*
import com.example.tp_jallon_virgile.databinding.FragmentTaskListBinding
import com.example.tp_jallon_virgile.form.FormActivity
import com.example.tp_jallon_virgile.network.Api
import com.example.tp_jallon_virgile.network.TasksRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import coil.load
import coil.transform.CircleCropTransformation
import com.example.tp_jallon_virgile.user.UserInfoActivity
import kotlinx.coroutines.flow.collectLatest

object TasksDiffCallback : DiffUtil.ItemCallback<Task>() {
    override fun areItemsTheSame(oldItem: Task, newItem: Task) =  oldItem.id == newItem.id;
    override fun areContentsTheSame(oldItem: Task, newItem: Task) =oldItem.equals(newItem);
}

class TaskListFragment : Fragment() {

    private lateinit var binding: FragmentTaskListBinding
    private val tasksRepository = TasksRepository()
    private val adapter = TaskListAdapter(createListener())
    private val viewModel: TaskListViewModel by viewModels()

    // Définit des launcher qui serviront à ajouter ou à éditer une tâche
    val formLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        val task = result.data?.getSerializableExtra("task") as? Task
        if (task != null) {
            lifecycleScope.launch{viewModel.addTask(task)
            }
        }
    }
    val formLauncher_edit = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        val task = result.data?.getSerializableExtra("task") as? Task
        if (task != null) {
            lifecycleScope.launch{viewModel.editTask(task)
            }
        }
    }

    // Définit le comportement des boutons
    fun createListener(): TaskListListener {
        val listener = object :  TaskListListener{
            override fun onClickDelete(task: Task) {
                viewModel.deleteTask(task)
                viewModel.loadTasks()
            }
            override fun onClickEdit(task: Task) {
                val intent = Intent(activity, FormActivity::class.java)
                intent.putExtra("task", task)
                formLauncher_edit.launch(intent)
            }
        }
        return listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTaskListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = adapter

        // Définit le comportement du bouton Add
        val button = binding.floatingActionButton
        button.setOnClickListener {
            val intent = Intent(activity, FormActivity::class.java)
            formLauncher.launch(intent)
        }
        // Définit le comportement à adopter lors du clic sur l'image de profil
        val profilePicture = binding.profileImage;
        profilePicture.setOnClickListener{
            val intent = Intent(activity, UserInfoActivity::class.java)
            formLauncher.launch(intent)
        }

        // On lance la coroutine
        lifecycleScope.launch {
            viewModel.taskList.collectLatest {newList ->
                adapter.submitList(newList.toList())
            }
        }
    }


    // On récupère les infos de l'utilisateur puis on affiche les taches
    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            val userInfo = Api.userWebService.getInfo().body()!!
            binding.userInfoTextView.text = "${userInfo.firstName} ${userInfo.lastName}"
            // Récupère l'image de profil et la rogne selon un cercle
            binding.profileImage.load(userInfo.avatar){
                transformations(CircleCropTransformation())
                error(R.drawable.ic_launcher_background)
            }
            viewModel.loadTasks()
        }
    }




}
