package com.example.tp_jallon_virgile.tasklist
import kotlinx.serialization.Serializable

// Note : Serializable permet à une classe d'être transférée via un réseau ou stockée dans une BDD

/**
 * Classe représentant une tâche
 */
@Serializable
data class Task (val id : String, val title : String, val description : String = "Une description par défaut") : java.io.Serializable