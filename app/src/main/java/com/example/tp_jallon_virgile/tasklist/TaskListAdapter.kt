import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_jallon_virgile.databinding.ItemTaskBinding
import com.example.tp_jallon_virgile.tasklist.Task
import com.example.tp_jallon_virgile.tasklist.TasksDiffCallback

interface TaskListListener {
    fun onClickDelete(task: Task)
    fun onClickEdit(task: Task)
}

class TaskListAdapter(val listener: TaskListListener) : ListAdapter<Task, TaskListAdapter.TaskViewHolder>(TasksDiffCallback) {

    inner class TaskViewHolder(private val binding: ItemTaskBinding) : RecyclerView.ViewHolder(binding.root) {
        // Valorise le titre et la description d'une tâche.
        // Définit le comportment à adopter lors d'un clic sur le bouton modifier et le bouton supprimer
        fun bind(task: Task) {
            binding.taskTitle.text = task.title
            binding.taskDesc.text = task.description
            binding.deleteButton.setOnClickListener{
                listener.onClickDelete(task)
            }
            binding.editButton.setOnClickListener{
                listener.onClickEdit(task)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemTaskBinding.inflate(inflater, parent, false)
        return TaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskListAdapter.TaskViewHolder, position: Int) {
        holder.bind(currentList[position])
    }
}