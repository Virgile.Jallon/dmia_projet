package com.example.tp_jallon_virgile.tasklist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tp_jallon_virgile.network.TasksRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

/**
 * Classe jouant un rôle à cheval entre un Contrôleur et un Manager
 * Permet de faire un CRUD sur les différentes tâches
 * Pour celà, fait appel aux méthdes CRUD du Manager
 */
class TaskListViewModel: ViewModel() {
    private val repository = TasksRepository()
    private val _taskList = MutableStateFlow<List<Task>>(emptyList())
    public val taskList: StateFlow<List<Task>> = _taskList

    // Charge les tasks
    fun loadTasks() {
        viewModelScope.launch {
            val taskResponse = repository.loadTasks()
            if(taskResponse != null){
                _taskList.value = taskResponse
            }
        }
    }

    // Supprime une tâche
    fun deleteTask(task: Task) {
        viewModelScope.launch {
            val taskResponse = repository.deleteTask(task)
            if(taskResponse) {
                _taskList.value = _taskList.value - task
            }
        }
    }

    // Crée une tâche
    fun addTask(task: Task) {
        viewModelScope.launch {
            val taskResponse = repository.createTask(task)
            if(taskResponse != null){
                _taskList.value = _taskList.value + taskResponse
            }
        }
    }

    // Modifie une tache
    fun editTask(task: Task) {
        viewModelScope.launch {
            val taskResponse = repository.updateTask(task)
            if(taskResponse != null){
                val oldTask = taskList.value.firstOrNull { it.id == taskResponse.id }
                if (oldTask != null) _taskList.value = taskList.value - oldTask + taskResponse
            }
        }
    }
}